# how to set-up & run the project
The following will guide you through setting up the project on your local PC.
# pre-requisites
+ php version >=7.0.0
+ Mysql version >=5.5+
+ composer. (https://getcomposer.org/download/)


```
+ An empty mysql database. Make sure that the mysql user configured has access to it

## Perform the following in your terminal
```sh
# clone from bitbucket
git clone https://gitlab.com/bencyn/arshgrades.git

# cd into the folder created. Defaults to bbk
cd arshgrades

# setup database credentials, by editing the .env

# install composer dependencies
composer install

# import database 

```